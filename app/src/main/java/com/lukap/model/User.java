package com.lukap.models;

public class User {
    String id;
    String name;
    String sname;
    String mail;

    public User() {

    }

    public User (String id ,String name, String sname, String email){
        this.id=id;
        this.name=name;
        this.sname=sname;
        this.mail=mail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }
}
