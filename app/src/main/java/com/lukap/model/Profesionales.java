package com.lukap.model;

import java.util.Date;

public class Profesionales {
    private String pid;
    private String Nombre;
    private String Apellido;
    private String Email;
    private String Password;
    private String Direccion;
    private String Telefono;
    private Date Fecha;
    private String Experiencia;
    private String Cargos;
    private String Token;
    private char Role;

    public Profesionales() {
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String apellido) {
        Apellido = apellido;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public Date getFecha() {
        return Fecha;
    }

    public void setFecha(Date fecha) {
        Fecha = fecha;
    }

    public String getExperiencia() {
        return Experiencia;
    }

    public void setExperiencia(String experiencia) {
        Experiencia = experiencia;
    }

    public String getCargos() {
        return Cargos;
    }

    public void setCargos(String cargos) {
        Cargos = cargos;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public char getRole() {
        return Role;
    }

    public void setRole(char role) {
        Role = role;
    }

    @Override
    public String toString() {
        return "Profesionales{" +
                "Nombre='" + Nombre + '\'' +
                '}';
    }
}
