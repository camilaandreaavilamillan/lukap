package com.lukap;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button mButtonIamProfessional;
    Button mButtonIamClient;
    SharedPreferences mPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPref = getApplicationContext().getSharedPreferences("typeUser",MODE_PRIVATE);
        SharedPreferences.Editor editor = mPref.edit();

        mButtonIamProfessional=findViewById(R.id.btnIamProfessional);
        mButtonIamClient=findViewById(R.id.btnIamClient);

        mButtonIamProfessional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("user","professional");
                editor.apply();
                goToSelectAuth();
            }
        });
        mButtonIamClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("user","client");
                editor.apply();
                goToSelectAuth();
            }
        });
    }
    private void goToSelectAuth() {
        Intent intent = new Intent(MainActivity.this , SelectOptionAuthActivity.class);
        startActivity(intent);
    }
}